package com.example.rx_java.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rx_java.R;
import com.example.rx_java.model.Movie;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieAdapterHolder> {

    private List<Movie> movies;

    public MovieAdapter(List<Movie> movies) {
        this.movies = movies;
    }

    @NonNull
    @Override
    public MovieAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie, parent, false);
        return new MovieAdapterHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAdapterHolder holder, int position) {
        Movie movie = movies.get(position);
        holder.title.setText(movie.getTitle());
        holder.overview.setText(movie.getOverview());
        holder.language.setText(movie.getLanguage());
        holder.releaseDate.setText(movie.getReleaseDate());
        holder.ratingBar.setRating(movie.getVoteAverage());
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    // método para ctualizar la lista.
    public void setData(List<Movie> movies) {
        this.movies = movies;
        notifyDataSetChanged();
    }

    protected class MovieAdapterHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private TextView overview;
        private TextView language;
        private TextView releaseDate;
        private RatingBar ratingBar;

        public MovieAdapterHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tv_title);
            overview = itemView.findViewById(R.id.tv_overview);
            language = itemView.findViewById(R.id.tv_language);
            releaseDate = itemView.findViewById(R.id.tv_release_date);
            ratingBar = itemView.findViewById(R.id.rating_bar);
        }
    }

}
