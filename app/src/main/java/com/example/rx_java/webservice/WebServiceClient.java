package com.example.rx_java.webservice;

import com.example.rx_java.model.ListMovies;

import retrofit2.Call;
import retrofit2.http.GET;

public interface WebServiceClient {

    @GET("/movie/popular")
    Call<ListMovies> getMoviesPopular();

}

// Seguir con video  https://www.youtube.com/watch?v=xDF4dpSbvhA&list=PLN5jmnTStJNisk9u30xTspqrmusSDKw7B&index=2
// https://developers.themoviedb.org/3/movies/get-popular-movies?api_key=8cd13f584a4b55e728297b531679c8c8
// API key: 8cd13f584a4b55e728297b531679c8c8
// URI para la petición: https://api.themoviedb.org/3/movie/popular?api_key=8cd13f584a4b55e728297b531679c8c8

