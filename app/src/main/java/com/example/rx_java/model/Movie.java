package com.example.rx_java.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Movie {

    @SerializedName("original_language")
    private String language;

    @SerializedName("original_title")
    private String title;

    @SerializedName("overview")
    private String overview;

    @SerializedName("release_date")
    private String releaseDate;

    @SerializedName("vote_average")
    private Float voteAverage;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Objects.equals(language, movie.language) && Objects.equals(title, movie.title) && Objects.equals(overview, movie.overview) && Objects.equals(releaseDate, movie.releaseDate) && Objects.equals(voteAverage, movie.voteAverage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(language, title, overview, releaseDate, voteAverage);
    }

    @Override
    public String toString() {
        return "Movie{" +
                "language='" + language + '\'' +
                ", title='" + title + '\'' +
                ", overview='" + overview + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                ", voteAverage=" + voteAverage +
                '}';
    }
    
}
