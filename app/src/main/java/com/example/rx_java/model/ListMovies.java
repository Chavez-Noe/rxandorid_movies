package com.example.rx_java.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class ListMovies {

    @SerializedName("results")
    private List<Movie> movies;

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListMovies that = (ListMovies) o;
        return Objects.equals(movies, that.movies);
    }

    @Override
    public int hashCode() {
        return Objects.hash(movies);
    }

    @Override
    public String toString() {
        return "ListMovies{" +
                "movies=" + movies +
                '}';
    }

}
